const fs = require('fs')
const path = require('path')


function fileRead(file) {
    return data = fs.promises.readFile(file, 'utf-8')
}




function writeFileInUpperCase(fileRead, writeFile) {
    return new Promise((resolve, reject) => {
        data = fs.promises.readFile(fileRead, 'utf-8')
            .then((data) => {
				let dataInUpperCase = data.toString().toUpperCase()
                fs.promises.writeFile(writeFile, dataInUpperCase)
				.then(() => {
                        resolve("file convert into upperCase!")
                        fs.promises.appendFile('../test/filenames.txt', writeFile.toString() + '\n')
                            .then((resolve) => console.log("uppercase file name append."))
                            .catch((err) => console.log(err))
                    })
                    .catch((err) => reject(err))
            })
    })
}




function writeSplitDataInLowerCase(readfile, writefile) {
    return new Promise((resolve, reject) => {
        data = fs.promises.readFile(readfile)
            .then((data) => {
                let dataInLower = data.toString().toLowerCase();
                splitData = dataInLower.split(".").join("\n");
                fs.promises.writeFile(writefile, splitData, 'utf-8')
                    .then(() => {
                        resolve("Data split into sentences in lowerCase!")
                        fs.promises.appendFile('../test/filenames.txt', writefile.toString() + '\n')
                            .then(() => console.log("split file name append."))
                            .catch((err) => console.log(err))
                    })
                    .catch((err) => reject(err))
            })
    })
}




function writeSortDataInFile(fileread, filewrite) {
    return new Promise((resolve, reject) => {
        data = fs.promises.readFile(fileread)
            .then((data) => {
                data = data.toString();
                let sortdata = data.split('\n').sort().join('\n');
                fs.promises.writeFile(filewrite, sortdata, 'utf-8')
                    .then(() => {
                        resolve("Data is Sorted!")
                        fs.promises.appendFile('../test/filenames.txt', filewrite.toString() + '\n')
                            .then(() => console.log("sort file name append."))
                            .catch((err) => console.log(err))
                    })
                    .catch((err) => reject(err))
            })
    })
}




function deleteAllFiles(readfile) {
    return new Promise((resolve, reject) => {
        data = fs.promises.readFile(readfile)
            .then((data) => {
                fileNameArray = data.toString().trim().split("\n")
                fileNameArray.forEach(data => {
                    fs.promises.unlink(data)
                        .then(() => resolve("All file deleted!"))
                        .catch((err) => reject(err))
                })
            })
    })
}



module.exports = { fileRead, writeFileInUpperCase, writeSplitDataInLowerCase, writeSortDataInFile, deleteAllFiles}