const fs = require('fs');
let path = require("path")


//let dir = "./dir"

const createRandomDirctory = function(dirPath){
	return new Promise((resolve,reject)=>{
		fs.promises.mkdir(dirPath)
		.then(()=> resolve("dirctory created"))
		.catch((err)=> reject(err) )
	})
}



function createJsonFile(filepath, data){
    return new Promise((resolve, reject) => {
        fs.promises.writeFile(filepath, data, 'utf-8')
        .then(() => resolve(filepath) )       
        .catch((err) => reject(err))
    })
}


function deleteFiles(filepath){
    return new Promise((resolve, reject) => {
        fs.promises.unlink(filepath)
        .then(() => resolve(filepath) )       
        .catch((err) => reject(err))
    })
}


module.exports = {createRandomDirctory,createJsonFile,deleteFiles}
